## Estructura de archivos


https://angular.io/guide/quickstart#project-file-review 

* `src/`

| **Archivo** | **Descripción** |
|-----------------|-------------------|
|`app/app.component.{ts,html,css,spec.ts}` | Sería el controlador principal.
|`app/app.module.ts` | Equivalente al `app.js` de Angular 1. Definición de la aplicación.
|`assets/` | Recursos multimedia y de cualquier tipo.
|`environments/*` | Configuración de los entornos de la app, por ejemplo, tener variables globales para producción y para desarrollo.
|`favicon.ico` | El favicon.
|`index.html` | El index de la app. Muy similar al de Precision.
|`main.ts` | "The main entry point for your app", como el "`bootstrap`" de la aplicación. No creo que sea muy necesario editarlo.
|`polyfills.ts` | Normalizar los niveles de soporte de los estándares web para los diferentes navegadores. 
|`styles.css` | Estilos CSS globales/generales
|`test.ts` | El entry point o "`bootstrap`" para las pruebas de unidad. 
|`tsconfig.{*}.json` | Configuraciones de TypeScript para esta aplicación. 


* `/` (algunos archivos ya se sabe para qué sirven, mostrar únicamente los desconocidos)

| **Archivo** | **Descripción** |
|-----------------|-------------------|
|`e2e/` | Pruebas end-to-end.
|`.angular-cli.json` | Configuración de Angular CLI.
|`*.conf.js` | Archivos de configuración.
|`tsconfig.json` | Configuración de TypeScript para el IDE.
|`tsling.json` | Configuración del TSLint, ejecutable desde `ng lint`.



## Equivalencias AngularJS <--> Angular

https://angular.io/guide/architecture


| **Concepto** | **Descripción** | **Equivalencia** |
|-----------------|-------------------|-----------|
|module | Usualmente llamado `AppModule` define el mecanismo que arranca la app. Un módulo tiene uno o muchos componentes (controladores). | module  
|component | Conjunto de vistas, servicios y demás para un componente de negocio de la app. | controller
 
