## Update NodeJS and NPM in Ubuntu 16.04


There are several ways to update both `node` and `npm` libraries. 
By far the easiest one is this:

Source: https://stackoverflow.com/questions/41195952/updating-nodejs-on-ubuntu-16-04


  1. Execute `sudo npm install -g n`
  2. Update `sudo n latest`
  3. Verify both versions with `node -v` and `npm -version`
  
Now we are ready to run our project! 
