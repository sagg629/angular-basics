/*import {Xmen} from './classes/xmen.class';
import {Villanos} from './classes/villanos.class';*/
import {Xmen, Villanos} from './classes';

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

let nombre: string = 'chemo';
let celular: number = 3172881084;
let isActive: boolean = true;
let fechaDeHoy: Date = new Date();
let cualquierDato: any = '123';

/**
 * Activar function
 * @param who (mandatory)
 * @param object (param with a default value if not defined when calling the fn)
 * @param moment (optional param)
 */
function activar(who: string, object: string = 'batiseñal', moment?: string) {
    let msg: string = `${who} ha activado el/la ${object}?`;

    if (moment) {
        msg += ` en la ${moment}`;
    }
    console.log(msg);
}

activar('chemo'); //chemo ha activado el/la batiseñal
activar('chemo', 'machete'); //chemo ha activado el/la machete
activar('chemo', 'batiseñal', 'mañana'); //chemo ha activado el/la batiseñal en la mañana

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

let promise1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('promise OK');
        resolve();
    }, 1500);
});
promise1.then(() => {
    console.log('fuck YEAH');
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

interface XmenInterface {
    nombre: string,
    poder: string
}

function enviarMision(ecsmen: XmenInterface) {
    console.log(`Enviando a ... ${ecsmen.nombre}`);
}

let guolberin: XmenInterface = {
    nombre: 'guolberin',
    poder: 'regeneracion'
};
enviarMision(guolberin);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class Avenger {
    name: string = 'N/A'; //default name
    team: string = undefined;
    realName: string = undefined;
    canFight: boolean = false;
    wonFights: number = 0;

    constructor(nombre: string, equipo: string, nombreReal: string) {
        this.name = nombre;
        this.team = equipo;
        this.realName = nombreReal;
    }
}

// {name: 'Antman', team: 'cap', realName: 'scott lang', canFight: false, wonFights: 0}
let antman: Avenger = new Avenger('Antman', 'cap', 'scott lang');

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

let wolverine = new Xmen('logan', 'wolverine');
wolverine.imprimir(); //'logan - wolverine'

let lex = new Villanos('lex luthor', 'conquistar el mundo');
lex.imprimirPlan(); //'El plan es ... conquistar el mundo'





