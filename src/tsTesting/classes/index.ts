/**
 * Typescript modules playground
 * This is the central point where I export classes
 * If I want a specific class from a file I perform ... import {Xmen, Villanos} from './classes';
 */
export {Xmen} from './xmen.class';
export {Villanos} from './villanos.class';
