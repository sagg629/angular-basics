import {Component} from '@angular/core';

@Component({
    selector: 'app-body',
    templateUrl: './body.component.html'
})
export class BodyComponent {
    showMSG: boolean = true;
    frase: any = {
        msj: 'Soy tu chemo d\'or',
        autor: 'Sergio A. Guzmán'
    };
    apodos: string[] = ['Chemo', 'Padre', 'Checho', '107', 'Fríjol'];
}
