# Angular7 Basics

Autor: **Sergio A. Guzmán <sagg629@gmail.com>**, 05 de marzo de 2018 (retoma enero 2019)


## Links de interés
 * Curso principal
   * [Udemy](https://www.udemy.com/angular-2-fernando-herrera/)
 * Documentación principal
   * Angular CLI ([documentación](https://angular.io/cli) y [Github](https://github.com/angular/angular-cli))
   * [Angular quickstart](https://angular.io/guide/quickstart)
   * [Actualizar](https://update.angular.io/) a una versión reciente de Angular.


## Introducción
Este es un proyecto mascota el cual me permitirá aprender y conocer acerca de
Angular 7.

Es relativamente fácil asociar diferentes conceptos viniendo de una base de AngularJS.
Como también he trabajado un poco con [React](https://bitbucket.org/sagg629/react-basics)
y con [React Native](https://bitbucket.org/sagg629/react-native-basics) se comparten varios
conceptos, como el trabajar con TypeScript, la modularización y el sistema de componentes.


## Objetivos
La idea es empezar a asociar algunas características nuevas con las ya conocidas desde AngularJS 
(p.ej. `ng-repeat` es `*ngFor`).

Así mismo la inclusión de algunos comentarios en los archivos para indicar
y recordar qué es y qué hace algo en particular.

También sería interesante ir conociendo cosas de pruebas (E2E / Karma / .spec.js)
y pre-procesadores CSS.

Inclusión de librerías conocidas y revisar compatibilidad con librerías / código
de JavaScript conocido.


## ¡A trabajar!
Este proyecto fue generado con Angular CLI.

La gestión de una aplicación, sus módulos, componentes y demás, se rige a través de **Angular CLI** 
el cual es un completo toolset para crear aplicaciones, correr pruebas, 
revisar configuraciones especiales, entre otros.


## Pre-requisitos
Instalar NodeJS y NPM en caso de que no estén. Si ya están instalados verificar que
estén en sus más recientes versiones:
* **NodeJS** versión mínima `6.9.x` / verificar versión `node -v`
* **npm** versión mínima `3.x.x` / verificar versión `npm -version` o `npm -v`

Para más información sobre actualizar las librerías revisar archivo `notes/UpgradeNodeNPM.md`.


Ahora instalar **Angular CLI** globalmente (anteponer `sudo` si fuera necesario):
```
npm install -g @angular/cli
```
**NOTA** Si ya Angular CLI estaba instalado actualizar con `ng update @angular/cli`

Ahora estamos listos para ejecutar la aplicación con `npm run start`. 
Recomendado revisar todo el proyecto y este archivo para más información.


## Creación de la app
```
ng new nombre-applicación (parámetros opcionales)
```
En la documentación de Angular CLI se encuentran 
los distintos parámetros (opcionales) que pueden acompañar la línea de comando principal
de `ng new`. Algunos ejemplos:

* **Parámetro `--minimal`**: crea una aplicación muy sencilla, sin algunas 
dependencias de Karma, sin pruebas pre-definidas. Los detalles del `app.component`
están todos en un mismo archivo (HTML, estilos y controlador).


## Servidor de desarrollo
```
ng serve
```
Ir a `http://localhost:4200/`. La aplicación se recargará autom. cuando hayan cambios.

Ejecuta la aplicación localmente y sirve los archivos. Esta línea de comando
tiene parámetros interesantes, por ejemplo:

* **Parámetro `--host`**: el host, `localhost` por defecto.
* **Parámetro `--port`**: el puerto, 4200 por defecto.
* **Parámetro `--open`**: abrir con el explorador predeterminado, `false` por defecto.
* **Parámetro `--live-reload`**: recargar autom. cuando hay cambios, `true` por defecto.


## Code scaffolding
Ejecutar `ng generate component component-name` para generar un nuevo componente. 
Se puede usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.


## Build (producción)
```
ng build
```
Cuando se genera un build a producción, se genera contenido estático, 
es decir puede ser servido **sin necesidad de tener un backend que lo soporte**.
Todo será almacenado en el directorio `dist/`. Use el parámetro `-prod` para generar un build a producción.
 

## Correr pruebas de unidad
Ejecutar `ng test` para correr pruebas vía [Karma](https://karma-runner.github.io).


## Correr pruebas end-to-end
Ejecutar `ng e2e` para correr pruebas end-to-end vía [Protractor](http://www.protractortest.org/).

## Más información
Para mayor información de Angular CLI ejecutar `ng help` 
o visitar la documentación (ver sección "Links de interés")
